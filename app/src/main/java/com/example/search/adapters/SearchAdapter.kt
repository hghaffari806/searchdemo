package com.example.search.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.search.databinding.AboutQueryLayoutBinding
import com.example.search.databinding.SearchAdapterItemBinding
import com.example.search.models.SearchAdapterItem
import com.example.search.models.Types
import com.example.search.utils.recyclerview.SearchBaseViewHolder

class SearchAdapter(
    private val callback: (item: SearchAdapterItem) -> Unit
) : RecyclerView.Adapter<SearchBaseViewHolder>() {
    private val items = ArrayList<SearchAdapterItem>()


    @SuppressLint("NotifyDataSetChanged")
    fun addItems(list: List<SearchAdapterItem>) {
        items.clear()
        items.addAll(list)
        notifyDataSetChanged()
    }


    inner class DuckDuckGoHeadingViewHolder(private val binding: AboutQueryLayoutBinding) : SearchBaseViewHolder(binding.root) {

        override fun bind(item: SearchAdapterItem) {
            binding.aboutTitle.text = item.title
            binding.aboutText.text = item.text
            binding.aboutEntity.text = item.entity
            binding.aboutVisitPage.setOnClickListener {
                callback.invoke(item)
            }
        }

    }

    inner class DuckDuckGoItemViewHolder(private val binding: SearchAdapterItemBinding) : SearchBaseViewHolder(binding.root) {

        override fun bind(item: SearchAdapterItem) {
            binding.itemTitle.text = item.title
            binding.itemText.text = item.text

            binding.root.setOnClickListener {
                callback.invoke(item)
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchBaseViewHolder {
        if (viewType == 1) {
            return DuckDuckGoItemViewHolder(SearchAdapterItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ))
        }

        return DuckDuckGoHeadingViewHolder(AboutQueryLayoutBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ))
    }

    override fun onBindViewHolder(holder: SearchBaseViewHolder, position: Int) {
        val item = items[holder.absoluteAdapterPosition]
        holder.bind(item)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (items[position].type == Types.Item) 1
        else 0
    }

}