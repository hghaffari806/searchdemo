package com.example.search.data.remote.duck

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface DuckDuckGoApi {

    @GET
    suspend fun search(
        @Url url: String,
        @Query("skip_disambig") skip_disambig: Int = 1,
        @Query("no_html") no_html: Int = 1,
        @Query("format") format: String = "json",
        @Query("q") query: String
    ): Response<DuckDuckGoResult>
}