package com.example.search.data.remote.google

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface GoogleApi {
    @GET
    suspend fun search(
        @Url url: String,
        @Query("q") query: String,
        @Query("location") location: String = "Austin, Texas, United States",
        @Query("hl") h1: String = "en",
        @Query("gl") gl: String = "us",
        @Query("google_domain") googleDomain: String = "google.com",
        @Query("api_key") apiKey: String = "9e29c0840545d9c90b26b25800db6ccec6f436036b50c023a3ddabd4a0aabe8d",
    ): Response<GoogleResult>
}