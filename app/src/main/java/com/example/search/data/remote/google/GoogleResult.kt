package com.example.search.data.remote.google

data class GoogleResult(
    val organic_results: List<OrganicResult>
)