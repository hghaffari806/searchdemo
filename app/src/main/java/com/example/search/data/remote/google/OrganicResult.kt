package com.example.search.data.remote.google

data class OrganicResult(
    val about_page_link: String,
    val cached_page_link: String,
    val displayed_link: String,
    val displayed_results: String,
    val link: String,
    val position: Int,
    val snippet: String,
    val snippet_highlighted_words: List<String>,
    val title: String
)