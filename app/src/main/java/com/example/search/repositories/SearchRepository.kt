package com.example.search.repositories

import com.example.search.data.remote.Resource
import kotlinx.coroutines.flow.Flow

interface SearchRepository<T> {
    suspend fun searchTheQuery(query: String): Flow<Resource<T>>
}