package com.example.search.ui.duckduckgo

import com.example.search.data.remote.duck.DuckDuckGoResult

sealed class DuckDuckGoEvents(
    val duckDuckGoResult: DuckDuckGoResult? = null
) {
    class OnLoading : DuckDuckGoEvents()
    class OnSearchResult(duckDuckGoResult: DuckDuckGoResult) : DuckDuckGoEvents(duckDuckGoResult = duckDuckGoResult)
    class OnError : DuckDuckGoEvents()
}
