package com.example.search.ui.duckduckgo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.search.R
import com.example.search.adapters.SearchAdapter
import com.example.search.data.remote.duck.DuckDuckGoResult
import com.example.search.databinding.FragmentDuckDuckGoBinding
import com.example.search.models.SearchAdapterItem
import com.example.search.utils.IntentHelper
import com.example.search.utils.ItemConverter
import com.example.search.utils.extensions.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DuckDuckGoFragment : Fragment() {
    private val binding by lazy(LazyThreadSafetyMode.NONE) {
        FragmentDuckDuckGoBinding.inflate(layoutInflater)
    }
    private val viewModel: DuckDuckGoViewModel by viewModels()
    private lateinit var mAdapter: SearchAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupSearchView()
        setEventsListener()
    }

    private fun setupSearchView() {
        binding.searchViewLayout.searchView.onSearchAction {
            viewModel.search(it)
            hideKeyboard()
        }
    }

    private fun setupRecyclerView() {
       mAdapter = SearchAdapter {
           IntentHelper.openUrl(requireContext(), it.url)
       }

        with(binding.resultsRecyclerView) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mAdapter
        }
    }

    private fun setEventsListener() {
        lifecycleScope.launchWhenStarted {
            viewModel.eventsFlow.collect {
                when(it) {
                    is DuckDuckGoEvents.OnLoading -> binding.loading.show()

                    is DuckDuckGoEvents.OnSearchResult -> onSearchResult(it.duckDuckGoResult!!)

                    is DuckDuckGoEvents.OnError -> requireContext().showToast(R.string.something_went_wrong)
                    else -> {}
                }
            }
        }
    }

    private fun onSearchResult(duckDuckGoResult: DuckDuckGoResult) {
        binding.loading.hide()
        val relatedTopics = ItemConverter.convertToAdapterItem(duckDuckGoResult.relatedTopics)
        val results = ItemConverter.convertToAdapterItem(duckDuckGoResult.results)
        val itemsToShow = ArrayList<SearchAdapterItem>().apply {
            addAll(relatedTopics)
            addAll(results)
        }
        ItemConverter.convertToAdapterItem(duckDuckGoResult)?.let {
            itemsToShow.add(0, it)
        }
        mAdapter.addItems(itemsToShow)
    }

}