package com.example.search.ui.google

import com.example.search.data.remote.google.GoogleResult

sealed class GoogleEvents(
    val googleResult: GoogleResult? = null
) {
    class OnLoading : GoogleEvents()
    class OnSearchResult(goResult: GoogleResult) : GoogleEvents(googleResult = goResult)
    class OnError : GoogleEvents()
}
