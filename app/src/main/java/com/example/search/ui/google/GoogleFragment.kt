package com.example.search.ui.google

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.search.R
import com.example.search.adapters.SearchAdapter
import com.example.search.data.remote.google.GoogleResult
import com.example.search.databinding.FragmentGoogleBinding
import com.example.search.utils.IntentHelper
import com.example.search.utils.ItemConverter
import com.example.search.utils.extensions.*
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GoogleFragment : Fragment() {
    private val binding by lazy(LazyThreadSafetyMode.NONE) {
        FragmentGoogleBinding.inflate(layoutInflater)
    }
    private val viewModel: GoogleViewModel by viewModels()
    lateinit var mAdapter: SearchAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupRecyclerView()
        setupSearchView()
        setEventsListener()
    }

    private fun setupSearchView() {
        binding.searchViewLayout.searchView.onSearchAction {
            viewModel.search(it)
            hideKeyboard()
        }
    }

    private fun setupRecyclerView() {
        mAdapter = SearchAdapter {
            IntentHelper.openUrl(requireContext(), it.url)
        }

        with(binding.resultsRecyclerView) {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = mAdapter
        }
    }

    private fun setEventsListener() {
        lifecycleScope.launchWhenStarted {
            viewModel.eventFlow.collect {
                when(it) {
                    is GoogleEvents.OnLoading -> binding.loading.show()

                    is GoogleEvents.OnSearchResult -> onSearchResult(it.googleResult!!)

                    is GoogleEvents.OnError -> requireContext().showToast(R.string.something_went_wrong)
                    else -> {}
                }
            }
        }
    }

    private fun onSearchResult(googleResult: GoogleResult) {
        binding.loading.hide()
        val results = ItemConverter.convertGoogleResultToAdapterItem(googleResult.organic_results)
        mAdapter.addItems(results)
    }
}