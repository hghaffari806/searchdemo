package com.example.search.ui.google

import com.example.search.data.remote.Resource
import com.example.search.data.remote.google.GoogleApi
import com.example.search.data.remote.google.GoogleResult
import com.example.search.repositories.SearchRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GoogleRepositoryImpl @Inject constructor(
    private val api: GoogleApi
) : SearchRepository<GoogleResult> {

    override suspend fun searchTheQuery(query: String): Flow<Resource<GoogleResult>> = flow {
        emit(Resource.Loading())
        val result = api.search(url = "https://serpapi.com/search.json", query = query)
        if (result.isSuccessful) {
            val body = result.body()!!
            emit(Resource.Success(body))
            return@flow
        }
        emit(Resource.Error())
    }
}