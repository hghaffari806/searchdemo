package com.example.search.utils

import android.util.Log

object Logger {

    fun log(msg: String) {
        Log.e("Logger", msg)
    }

}