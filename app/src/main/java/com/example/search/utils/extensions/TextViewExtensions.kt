package com.example.search.utils.extensions

import android.view.inputmethod.EditorInfo
import android.widget.TextView

fun TextView.onSearchAction(callback: (text: String) -> Unit) {
    setOnEditorActionListener { tv1, p1, _ ->
        if (p1 == EditorInfo.IME_ACTION_SEARCH) {
            callback.invoke(tv1.text.toString())
        }
        false
    }
}