package com.example.search.utils.recyclerview

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.search.models.SearchAdapterItem

abstract class SearchBaseViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    abstract fun bind(item: SearchAdapterItem)

}